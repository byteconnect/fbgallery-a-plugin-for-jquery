/*!
 * FBGallery - A plugin for jQuery v0.9
 * https://bitbucket.org/byteconnect/fbgallery-a-plugin-for-jquery
 *
 * Copyright 2013 by Byte Connect GmbH (www.byte-connect.de)
 * Released under the MIT license
 * https://bitbucket.org/byteconnect/fbgallery-a-plugin-for-jquery/wiki/MIT-License
 */
(function ($) {

    // declare objects if necessary ------------------------------------------------------------
    if (typeof FBGallery === "undefined") {
        FBGallery = function (/*Node*/ rootNode, /*Object*/ options) {
            this.options = options;
            this.$root = $(rootNode);
            this.albums = [];
            this.cachedAlbums = {};
            this.actualAlbum = null;
            this.overlay = null;
            this.singleImageContainer = null;
            this.isPanelShown = false;
            this.isInteractionBlocked = false;
        };
        FBGallery.prototype.init = function () {
            console.log("loading photos from facebook ...");
            var self = this; //prevent correct scope for closures
            self.$root.html("");

            $("<div>")
                .addClass("fbg-loading-albums")
                .text("loading albums from facebook ...")
                .appendTo(self.$root);

            self._createOverlay();
            self._createSingleImageContainer();
            self.handleWindowResize();
            $(window).resize(function (event) {
                self.handleWindowResize();
            });
            $(document).keypress(function (event) {
                self._handleKey(event);
            });

            $.ajax({
                type:"GET",
                url:"http://graph.facebook.com/" + self.options.pageId + "/albums",
                data:{
                    fields:"id,name,type,count,cover_photo"
                },
                dataType:"jsonp",
                success:function (data) {
                    // filter albums
                    var dataArray = data.data;
                    for (var i = 0; i < dataArray.length; i++) {
                        if (dataArray[i].type === "normal" && typeof dataArray[i].cover_photo != "undefined") {
                            self.albums.push(dataArray[i]);
                        }
                    }
                    console.log("photos from facebook loaded");
                    self.createAlbumView();
                },
                error:function (xhr, type, error) {
                    window.alert(type + ": " + error);
                }
            });
        };
        FBGallery.prototype.createAlbumView = function () {
            console.log("creating album view ...");
            $(window).scrollTop(0);
            var self = this;
            self.$root.html("");

            var $albumContainer = $("<div>")
                .addClass("fbg-album-container")
                .appendTo(self.$root);

            for (var i = 0; i < self.albums.length; i++) {
                var album = self.albums[i];
                console.log("adding album %s ...", album.name);

                var $albumItem = $("<div>")
                    .addClass("fbg-album-item")
                    .data("index", i)
                    .click(function (event) {
                        var albumIdx = $(event.currentTarget).data("index");
                        var actAlbum = self.albums[albumIdx];
                        if (typeof self.cachedAlbums[actAlbum.id] === "undefined") {
                            var fbGalleryAlbum = new FBGalleryAlbum(actAlbum, self);
                            self.cachedAlbums[actAlbum.id] = fbGalleryAlbum;
                            fbGalleryAlbum.show();
                        } else {
                            self.cachedAlbums[actAlbum.id].show();
                        }
                        self.actualAlbum = self.cachedAlbums[actAlbum.id];
                    });

                var albumLabel = album.name.length > 15 ? album.name.substr(0, 15) + (" ...") : album.name;
                var $albumImg = $("<img>")
                    .attr("src", "https://graph.facebook.com/" + album.cover_photo + "/picture?type=album")
                    .addClass("fbg-album-item-img")
                    .attr("title", album.name)
                    .attr("alt", "image for album " + albumLabel)
                    .appendTo($albumItem);
                var $albumInfo = $("<div>")
                    .addClass("fbg-album-item-info").appendTo($albumItem);

                $("<span>")
                    .addClass("fbg-album-item-name")
                    .text(albumLabel)
                    .attr("title", album.name)
                    .appendTo($albumInfo);
                var photoCount = album.count * 1;
                $("<span>")
                    .addClass("fbg-album-item-count")
                    .text("(" + album.count + (photoCount > 1 ? " photos)" : " photo)"))
                    .appendTo($albumInfo);

                $albumItem.appendTo($albumContainer);
            }
            console.log("album view created");
        };
        FBGallery.prototype.getWindowSize = function () {
            var winW = 0,
                winH = 0;
            if (typeof (window.innerWidth) == "number") {
                winW = window.innerWidth;
                winH = window.innerHeight
            } else {
                if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                    winW = document.documentElement.clientWidth;
                    winH = document.documentElement.clientHeight
                } else {
                    if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
                        winW = document.body.clientWidth;
                        winH = document.body.clientHeight
                    }
                }
            }
            return {
                width:winW,
                height:winH
            }
        };
        FBGallery.prototype._createOverlay = function () {
            var self = this;
            self.overlay = $("<div>")
                .attr("id", "fbg-overlay")
                .css({
                    opacity:0,
                    zIndex:1000
                })
                .hide()
                .click(function (event) {
                    self._hideSingleImage();
                })
                .appendTo("body");
            console.log("overlay created");
        };
        FBGallery.prototype._createSingleImageContainer = function () {
            var self = this;
            var $container = $("<div>")
                .attr("id", "fbg-single-photo-container")
                .css({
                    zIndex:1001
                })
                .click(function (event) {
                    self._hideSingleImage();
                })
                .hide()
                .appendTo("body");
            var $innerContainer = $("<div>")
                .attr("id", "fbg-single-photo-inner-container")
                .appendTo($container);
            var $imageContainer = $("<div>")
                .attr("id", "fbg-single-photo-image-container")
                .appendTo($innerContainer);
            var $singleImg = $("<img>")
                .attr("id", "fbg-single-photo-image")
                .attr("title", "Large image")
                .attr("alt", "large image view")
                .hide()
                .appendTo($imageContainer);
            var $controlContainer = $("<div>")
                .attr("id", "fbg-single-photo-image-controls")
                .appendTo($imageContainer);
            var $controlPrev = $("<a>")
                .attr("id", "fbg-single-photo-image-prev")
                .attr("href", "#")
                .click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    self.actualAlbum.showPrevious();
                })
                .appendTo($controlContainer);
            var $controlNext = $("<a>")
                .attr("id", "fbg-single-photo-image-next")
                .attr("href", "#")
                .click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    self.actualAlbum.showNext();
                })
                .appendTo($controlContainer);

            var $dataContainer = $("<div>")
                .addClass("fbg-single-photo-data-container")
                .hide()
                .appendTo($container);

            var $info = $("<div>")
                .addClass("fbg-single-photo-info")
                .appendTo($dataContainer);

            $("<div>")
                .addClass("fbg-single-photo-close")
                .click(function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    self._hideSingleImage();
                })
                .appendTo($dataContainer);

            self.singleImageContainer = $container;
            self.singleImageContainer.inner = $innerContainer;
            self.singleImageContainer.controlPrev = $controlPrev;
            self.singleImageContainer.controlNext = $controlNext;
            self.singleImageContainer.singleImg = $singleImg;
            self.singleImageContainer.dataContainer = $dataContainer;
            self.singleImageContainer.info = $info;
            console.log("single image container created");
        };
        FBGallery.prototype._handleKey = function (event) {
            var self = this;
            if (self.isPanelShown) {
                if (event.keyCode === 27) {
                    self._hideSingleImage();
                } else if (event.keyCode === 37) {
                    self.actualAlbum.showPrevious();
                } else if (event.keyCode === 39) {
                    self.actualAlbum.showNext();
                }
            }
        };
        FBGallery.prototype.handleWindowResize = function () {
            var self = this;
            if (self.isPanelShown) {
                var self = this;
                var size = self.getWindowSize();
                self.overlay.css({
                    width:size.width,
                    height:size.height
                });
                self.singleImageContainer.css({
                    width:size.width
                });
                console.log("window resized");
            }
        };
        FBGallery.prototype._hideSingleImage = function () {
            var self = this;

            if (self.isInteractionBlocked) return;

            console.log("hiding single image ...");
            self.overlay
                .animate({
                    opacity:0
                }, self.options.effectDuration, function () {
                    self.overlay.hide();
                    self.isPanelShown = false;
                    self.isInteractionBlocked = false;
                });
            self.singleImageContainer.dataContainer.hide();
            self.singleImageContainer.fadeOut(self.options.effectDuration, function () {
                self.singleImageContainer.singleImg.hide();
            });
        };

        FBGalleryAlbum = function (/*Object*/ album, /*Object*/ fbGallery) {
            this.fbGallery = fbGallery;
            this.album = album;
            this.photos = {};
            this.loaded = false;
            this.actImgIdx = 0;
        };
        FBGalleryAlbum.prototype.show = function () {
            var self = this;
            if (!self.loaded) {
                console.log("loading album photos from facebook ...");
                self.fbGallery.$root.html("");
                $("<div>")
                    .addClass("fbg-loading-photos")
                    .text("loading album photos from facebook ...")
                    .appendTo(self.fbGallery.$root);

                $.ajax({
                    type:"GET",
                    url:"http://graph.facebook.com/" + self.album.id + "/photos",
                    data:{
                        limit:100
                    },
                    dataType:"jsonp",
                    success:function (data) {
                        self.photos = data.data;
                        console.log("album photos from facebook loaded");
                        self.loaded = true;
                        self._show();
                    },
                    error:function (xhr, type, error) {
                        window.alert(type + ": " + error);
                    }
                });
            } else {
                self._show();
            }
        };
        FBGalleryAlbum.prototype.showPrevious = function () {
            var self = this;

            if (self.fbGallery.isInteractionBlocked) return;

            if (self.actImgIdx > 0) {
                self.actImgIdx--;
                console.log("showing previous image #%s", self.actImgIdx);
                self._showSingleImage();
            }
        };
        FBGalleryAlbum.prototype.showNext = function () {
            var self = this;

            if (self.fbGallery.isInteractionBlocked) return;

            if (self.actImgIdx < self.photos.length - 1) {
                self.actImgIdx++;
                console.log("showing next image #%s", self.actImgIdx);
                self._showSingleImage();
            }
        };
        FBGalleryAlbum.prototype._show = function () {
            var self = this;
            console.log("creating photo view for album %s ...", self.album.name);
            $(window).scrollTop(0);
            self.fbGallery.$root.html("");

            var $photoControls = $("<div>")
                .addClass("fbg-photo-controls")
                .appendTo(self.fbGallery.$root);
            $("<a>")
                .addClass("fbg-photo-controls-back")
                .attr("href", "#")
                .text("back to albums")
                .click(function (event) {
                    event.preventDefault();
                    self.fbGallery.createAlbumView();
                })
                .appendTo($photoControls);
            $("<div>")
                .addClass("fbg-photo-controls-header")
                .text(self.album.name)
                .appendTo($photoControls);
            var photoCount = self.album.count * 1;
            $("<div>")
                .addClass("fbg-photo-controls-count")
                .text("(" + self.album.count + (photoCount > 1 ? " photos)" : " photo)"))
                .appendTo($photoControls);

            var $photoContainer = $("<div>")
                .addClass("fbg-photo-container")
                .appendTo(self.fbGallery.$root);

            for (var i = 0; i < self.photos.length; i++) {
                // get 180x180 url
                var photo = self.photos[i].images[6];

                var $photoItem = $("<div>")
                    .addClass("fbg-photo-item")
                    .data("index", i)
                    .click(function (event) {
                        self.actImgIdx = $(event.currentTarget).data("index") * 1;
                        self._showSingleImage();
                    });
                var $photoImg = $("<img>")
                    .attr("src", photo.source)
                    .addClass("fbg-photo-item-img")
                    .attr("alt", "photo image #" + i)
                    .appendTo($photoItem);
                $photoItem.appendTo($photoContainer);
            }

            var $photoFooter = $("<div>")
                .addClass("fbg-photo-footer")
                .appendTo(self.fbGallery.$root);
            $("<a>")
                .addClass("fbg-photo-footer-back")
                .attr("href", "#")
                .text("back to albums")
                .click(function (event) {
                    event.preventDefault();
                    self.fbGallery.createAlbumView();
                })
                .appendTo($photoFooter);

            console.log("photo view created");
        };
        FBGalleryAlbum.prototype._showSingleImage = function () {
            var self = this;

            if (self.fbGallery.isInteractionBlocked) return;

            self.fbGallery.isInteractionBlocked = true;

            var imgArray = self.photos[self.actImgIdx].images;
            var imgObj = imgArray[1];
            var size = self.fbGallery.getWindowSize();

            console.log("showing single image (%s) ...", imgObj.source);

            // choose highest possible image resolution
            for (var j = 0; j < imgArray.length; j++) {
                if (imgArray[j].width < size.width && imgArray[j].height < (size.height - 50)) {
                    imgObj = imgArray[j];
                    break;
                }
            }

            // set control buttons
            if (self.actImgIdx >= self.photos.length - 1) {
                self.fbGallery.singleImageContainer.controlNext.hide();
            } else {
                self.fbGallery.singleImageContainer.controlNext.show();
            }
            if (self.actImgIdx <= 0) {
                self.fbGallery.singleImageContainer.controlPrev.hide();
            } else {
                self.fbGallery.singleImageContainer.controlPrev.show();
            }

            if (!self.fbGallery.isPanelShown) {
                // show overlay
                self.fbGallery.isPanelShown = true;
                self.fbGallery.handleWindowResize();
                self.fbGallery.overlay
                    .show()
                    .animate({
                        opacity:self.fbGallery.options.overlayOpacity
                    }, self.fbGallery.options.effectDuration);

                // show image container
                self.fbGallery.singleImageContainer.fadeIn(self.fbGallery.options.effectDuration, function () {
                    self._changeImage(imgObj);
                });
            } else {
                self.fbGallery.singleImageContainer.singleImg.hide();
                self._changeImage(imgObj);
            }


        };
        FBGalleryAlbum.prototype._changeImage = function (/*Object*/imgObj) {
            var self = this;

            self.fbGallery.singleImageContainer.dataContainer.hide();

            self.fbGallery.singleImageContainer.inner
                .addClass("fbg-single-photo-image-loading");

            var preloader = new Image();
            preloader.onload = function () {
                console.log("single photo loaded");
                self.fbGallery.singleImageContainer.inner
                    .animate({
                        width:imgObj.width,
                        height:imgObj.height
                    }, self.fbGallery.options.effectDuration,
                    function () {
                        self.fbGallery.singleImageContainer.singleImg
                            .attr("src", imgObj.source)
                            .show();
                        self.fbGallery.singleImageContainer.inner
                            .removeClass("fbg-single-photo-image-loading")

                        self.fbGallery.singleImageContainer.info.text("Image "
                            + (self.actImgIdx + 1)
                            + " of "
                            + self.album.count
                        );
                        self.fbGallery.singleImageContainer.dataContainer
                            .css({
                                width:imgObj.width
                            })
                            .show();
                        self.fbGallery.isInteractionBlocked = false;
                    });
            };
            preloader.onerror = function () {
                self.fbGallery.isInteractionBlocked = false;
                self.fbGallery.singleImageContainer.inner
                    .removeClass("fbg-single-photo-image-loading");
                window.alert("Error: Couldn't load single image!");
            };
            preloader.src = imgObj.source;
        };

        console.log("classes for fbgallery created");
    }

    // extend jQuery --------------------------------------------------------------------
    $.fn.fbgallery = function (options) {
        var settings = $.extend({
            effectDuration:500,
            overlayOpacity:0.8
        }, options);

        if (!settings.pageId) {
            window.alert("Facebook Galery: You didn't set the id for the facebook page (pageId)!");
            return;
        }

        return this.each(function () {
            var fbGallery = new FBGallery(this, settings);
            fbGallery.init();
        });
    };
})(jQuery);

/*
 Interesting links:
 ------------------------------------
 - https://developers.facebook.com/docs/reference/api/using-pictures/ (Facebook API)
 - http://msdn.microsoft.com/en-us/library/hh404088.aspx (JS-Testing with qUnit)
 - http://preloaders.net/ (gif-generator for loading images)
 */